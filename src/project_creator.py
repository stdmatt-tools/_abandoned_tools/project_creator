#!/usr/bin/env python3
##~---------------------------------------------------------------------------##
##                        _      _                 _   _                      ##
##                    ___| |_ __| |_ __ ___   __ _| |_| |_                    ##
##                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   ##
##                   \__ \ || (_| | | | | | | (_| | |_| |_                    ##
##                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   ##
##                                                                            ##
##  File      : project_creator.py                                            ##
##  Project   : project_creator                                               ##
##  Date      : Mar 01, 2020                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : stdmatt <stdmatt@pixelwizards.io>                             ##
##  Copyright : stdmatt 2020                                                  ##
##                                                                            ##
##  Description :                                                             ##
##                                                                            ##
##---------------------------------------------------------------------------~##

##----------------------------------------------------------------------------##
## Imports                                                                    ##
##----------------------------------------------------------------------------##
import argparse;
import os;
import os.path;
import shlex;
import subprocess;


##----------------------------------------------------------------------------##
## Constants                                                                  ##
##----------------------------------------------------------------------------##
PROGRAM_NAME = "project-creator";
PROGRAM_VERSION = "1.1.0";
PROGRAM_COPYRIGHT_YEARS = "2020";

ETC_PATH = "/usr/local/etc/stdmatt/project_creator";


##----------------------------------------------------------------------------##
## Globals                                                                    ##
##----------------------------------------------------------------------------##
is_debug = True;


##----------------------------------------------------------------------------##
## Git Functions                                                              ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
def git_exec(path, args):
    path           = normalize_path(path);
    cmd            = "git -C \"%s\" %s" % (path, args);
    cmd_components = shlex.split(cmd);
    log_debug("{0}", cmd);

    p = subprocess.Popen(cmd_components, stdout=subprocess.PIPE, stderr=subprocess.PIPE);
    output, errors = p.communicate();

    if(p.returncode):
        log_debug_error("Failed running {0}", cmd);

    return output.decode('utf-8').strip(" ").strip("\n"), p.returncode;


##----------------------------------------------------------------------------##
## Logging Functions                                                          ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
def log(prefix, fmt, *args):
    print("[{0}] {1}".format(
        prefix, fmt.format(*args)
    ));

##------------------------------------------------------------------------------
def log_debug_error(fmt, *args):
    if(is_debug):
        log("DEBUG-ERROR", fmt, *args);

##------------------------------------------------------------------------------
def log_debug(fmt, *args):
    if(is_debug):
        log("DEBUG", fmt, *args);

##------------------------------------------------------------------------------
def log_fatal(fmt, *args):
    log("FATAL", fmt, *args);
    exit(1);

##------------------------------------------------------------------------------
def log_info(fmt, *args):
    log("INFO", fmt, *args);


##----------------------------------------------------------------------------##
## Helper Functions                                                           ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
def get_help():
    msg = """
Usage:
  {program_name} [--help] [--version]
  {program_name} [--create] [--gitlab <owner>]
  {program_name} [--project-name <name>] [--root-path <path>]

  *--help    : Show this screen.
  *--version : Show program version and copyright.

  --create : Creates a project directory, initialize git repository
             and add the project files.

  --gitlab <owner> : Push the project to Gitlab under the owner name.

  --project-name <name> : Name that the project will be created.
                          Optional if just adding the project files, but
                          required if creating a new project.

  --root-path <path> : Path the the project will be created.

Notes:
  If <name> is blank the current directory basename(1) is assumed.
  If <path> is blank the current directory is assumed.

  Options marked with * are exclusive, i.e. the {program_name} will run that
  and exit after the operation.
    """;
    return msg.format(program_name=PROGRAM_NAME)


##------------------------------------------------------------------------------
def show_version():
    msg = """{program_name} - {program_version} - stdmatt <stdmatt@pixelwizards.io>
Copyright (c) {program_copyright_years} - stdmatt
This is a free software (GPLv3) - Share/Hack it
Check http://stdmatt.com for more :)""".format(
        program_name=PROGRAM_NAME,
        program_version=PROGRAM_VERSION,
        program_copyright_years=PROGRAM_COPYRIGHT_YEARS
    );

    print(msg);
    exit(0);


##------------------------------------------------------------------------------
def normalize_path(path):
    return os.path.abspath(os.path.normpath(os.path.expanduser(path)));

##------------------------------------------------------------------------------
def parse_cmdline():
    parser = argparse.ArgumentParser(
        usage=get_help(),
        add_help=False,
    );

    parser.add_argument(
        "--help",
        dest="help",
        action="store_true",
        default=False,
    );

    parser.add_argument(
        "--version",
        dest="version",
        action="store_true",
        default=False,
    );

    parser.add_argument(
        "--create",
        dest="create",
        action="store_true",
        default=False
    );

    parser.add_argument(
        "--project-name",
        dest="project_name",
        action="store",
        default=None
    );
    parser.add_argument(
        "--root-path",
        dest="root_path",
        action="store",
        default=None
    );

    parser.add_argument(
        "--gitlab",
        dest="gitlab",
        action="store",
        default=None
    );
    return parser.parse_args()

##------------------------------------------------------------------------------
def create_project_dir(fullpath):
    log_debug("Creating project directory at: ({0})", fullpath);
    os.makedirs(fullpath, exist_ok=True);

##------------------------------------------------------------------------------
def initialize_git_repository(fullpath):
    log_debug("Initializing git repository at ({0})", fullpath);
    git_exec(fullpath, "init");

##------------------------------------------------------------------------------
def add_missing_project_files(project_name, proj_fullpath):
    etc_files_list     = os.listdir(ETC_PATH);
    project_files_list = os.listdir(proj_fullpath);

    for etc_filename in etc_files_list:
        if(etc_filename not in project_files_list):
            log_debug("Missing project file: {0}", etc_filename);

            etc_full_filename    = os.path.join(ETC_PATH,      etc_filename);
            target_full_filename = os.path.join(proj_fullpath, etc_filename);

            with open(etc_full_filename, "r") as input_file:
                input_lines  = input_file.readlines();
                output_lines = expand_file_placeholders(input_lines, project_name);
                with open(target_full_filename, "w") as output_file:
                    output_file.writelines(output_lines);

##------------------------------------------------------------------------------
def expand_file_placeholders(lines, project_name):
    for i, line in enumerate(lines):
        if(line.find("$PROJECT_NAME") != -1):
            lines[i] = line.replace("$PROJECT_NAME", project_name);
    return lines;

##------------------------------------------------------------------------------
def create_gitlab_project(project_name, project_group, git_root_path):
    git_url = "https://gitlab.com/{project_group}/{project_name}.git".format(
        project_group=project_group,
        project_name=project_name
    );

    ## Push
    git_cmd = "push --set-upstream {git_url} master".format(git_url=git_url);
    result, errorcode = git_exec(
        git_root_path,
        git_cmd.format()
    )
    ## @TODO(stdmatt): Add origin as well.
    if(errorcode):
        log_fatal("Failed to create project at gitlab.\nError: {0}", result)

    ## Set upstream
    git_cmd = "remote add origin {git_url}".format(git_url=git_url);
    result, errorcode = git_exec(
        git_root_path,
        git_cmd.format()
    )
    ## @TODO(stdmatt): Add origin as well.
    if(errorcode):
        log_fatal("Failed to create project at gitlab.\nError: {0}", result)

##------------------------------------------------------------------------------
def create_project(project_name, root_path, gitlab_group):
    ##
    ## We creating the project from scratch...
    full_project_path = None;

    if(project_name is None):
        log_fatal("Create requires --project-name <name> to be set.");

    if(root_path is None):
        log_debug("root_path is not given - defaulting to cwd");
        root_path = ".";

    root_path         = normalize_path(root_path);
    full_project_path = os.path.join(root_path, project_name);

    if(os.path.exists(full_project_path)):
        log_fatal("Already exists a project at: {0}", full_project_path);

    create_project_dir       (full_project_path);
    initialize_git_repository(full_project_path);
    add_missing_project_files(project_name, full_project_path);

    if(gitlab_group is not None):
        create_gitlab_project(project_name, gitlab_group, full_project_path);

    exit(0);

##------------------------------------------------------------------------------
def update_project(project_name, root_path, gitlab_group):
    ##
    ## We're not creating from scratch but adding stuff...
    ## If users didn't give the root path try to infer.
    git_root_path     = None;
    tmp_git_root_path = ".";

    if(root_path is not None):
        tmp_git_root_path = root_path;

    tmp_git_root_path = normalize_path(tmp_git_root_path);

    result, errcode  = git_exec(tmp_git_root_path, "rev-parse --show-toplevel");
    if(errcode == 0):
        git_root_path = normalize_path(result);
        root_path     = normalize_path(result);
        log_info(
            "Found git repository, using it as root path.\n       Path: ({0})",
            git_root_path
        );

    if(root_path is None):
        log_fatal("Coudn't find the project root path - Aborting...");

    root_path = normalize_path(root_path);

    ## If users didn't give the project name try to infer.
    if(project_name is None):
        log_debug("project-name is not given trying to infer...");
        if(git_root_path is None):
            project_name = os.path.basename(root_path);
            log_debug(
                "git root path is not found - Assumming project_name as ({0})",
                project_name
            );
        else:
            log_debug("git root path is found - Trying to get the origin name");
            ## Try to get the origin name, if it fails use the
            ## directory name as the project name.
            result, errcode = git_exec(git_root_path, "remote -v");
            if(errcode != 0 or len(result) == 0):
                project_name = os.path.basename(root_path);
                log_debug(
                    "git origin name is not found - Assuming project_name as: ({0})",
                    project_name
                );
            else:
                origin       = result.splitlines(keepends=False)[0].split()[1];
                project_name = os.path.basename(origin.rstrip(".git"));
                log_debug(
                    "git origin name is found - Assuming project_name as: ({0})",
                    project_name
                );

    if(git_root_path is None):
        initialize_git_repository(root_path);

    add_missing_project_files(project_name, root_path);

    if(gitlab_group is not None):
        create_gitlab_project(project_name, gitlab_group, root_path);

    exit(0);


##----------------------------------------------------------------------------##
## Script                                                                     ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
def main():
    args = parse_cmdline();
    if(args.help):
        print(get_help())
        exit(0);
    if(args.version):
        show_version();

    should_create = args.create;

    project_name = args.project_name;
    root_path    = args.root_path;
    gitlab_group = args.gitlab;

    if(should_create):
        create_project(project_name, root_path, gitlab_group);
    else:
        update_project(project_name, root_path, gitlab_group);


if __name__ == "__main__":
    main();
