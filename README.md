# Project Creator

**Made with <3 by [stdmatt](http://stdmatt.com).**

<!--  -->
## Description:

```project-creator``` is a small utility to create a bare bones project structure.

It initialize a ```git``` repository - optionally creating it on [GitLab](http://gitlab.com)
and creates the standard files like ```AUTHORS.txt```, ```CHANGELOG.txt```,
```COPYING.txt```, etc.


```project-creator``` is not a fancy program, but helps to keep my projects
with a consistent layout and structure.

<br>

As usual, you are **very welcomed** to **share** and **hack** it.


<!--  -->
## Usage
```
Usage:
  project-creator [--help] [--version]
  project-creator [--create] [--gitlab <owner>]
  project-creator [--project-name <name>] [--root-path <path>]

  *--help    : Show this screen.
  *--version : Show program version and copyright.

  --create : Creates a project directory, initialize git repository
             and add the project files.

  --gitlab <owner> : Push the project to Gitlab under the owner name.

  --project-name <name> : Name that the project will be created.
                          Optional if just adding the project files, but
                          required if creating a new project.

  --root-path <path> : Path the the project will be created.

Notes:
  If <name> is blank the current directory basename(1) is assumed.
  If <path> is blank the current directory is assumed.

  Options marked with * are exclusive, i.e. the project-creator will run that
  and exit after the operation.
```

<!--  -->
## Installing:

There's a install script for that ;D
```
git clone https://gitlab.com/stdmatt-personal/project_creator.git
cd ./project_creator

./install.sh  ## It'll ask for the super user password.
```

<!--  -->
## License:

This software is released under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html).


<!--  -->
## Others:

There's more FLOSS things at [stdmatt.com](https://stdmatt.com) :)
